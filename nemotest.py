import copy
import os
import pickle

import torch
from torch.multiprocessing import Pool, Process, set_start_method

import numpy as np
from ruamel.yaml import YAML

import nemo
import nemo.collections.asr as nemo_asr
from nemo.collections.asr.helpers import post_process_predictions, post_process_transcripts, word_error_rate

from ruamel.yaml import YAML
from nemo.backends.pytorch.nm import DataLayerNM
from nemo.core.neural_types import * #NeuralType, BatchTag, TimeTag, AxisType

import scipy.io.wavfile as wave

# from multiprocessing import Pool

import time

# TODO: update to your checkpoints
CHECKPOINT_ENCODER = 'modelnemo/JasperEncoder-STEP-265520.pt'
CHECKPOINT_DECODER = 'modelnemo/JasperDecoderForCTC-STEP-265520.pt'

# TODO: update to your audio file

class AudioDataLayer(DataLayerNM):
    @property
    def output_ports(self):
        return {
            "audio_signal": NeuralType(('B', 'T'), AudioSignal(freq=16000)),
            "a_sig_length": NeuralType(tuple('B'), LengthsType()),
            # "audio_signal": NeuralType({0: AxisType(BatchTag),
            #                             1: AxisType(TimeTag)}),

            # "a_sig_length": NeuralType({0: AxisType(BatchTag)}),
        }

    def __init__(self, **kwargs):
        DataLayerNM.__init__(self, **kwargs)
        self.output = True
        
    def __iter__(self):
        return self
    
    def __next__(self):
        if not self.output:
            raise StopIteration
        self.output = False
        return torch.as_tensor(self.signal, dtype=torch.float32), \
               torch.as_tensor(self.signal_shape, dtype=torch.int64)
        
    def set_signal(self, signal):
        self.signal = np.reshape(signal.astype(np.float32)/32768., [1, -1])
        self.signal_shape = np.expand_dims(self.signal.size, 0).astype(np.int64)
        self.output = True

    def __len__(self):
        return 1

    @property
    def dataset(self):
        return None

    @property
    def data_iterator(self):
        return self


yaml = YAML(typ="safe")
with open("modelnemo/jasper10x5dr.yaml") as f:
    jasper_model_definition = yaml.load(f)

device = nemo.core.DeviceType.GPU

neural_factory = nemo.core.NeuralModuleFactory(
        backend=nemo.core.Backend.PyTorch,
        local_rank=None,
        optimization_level=nemo.core.Optimization.mxprO1,
        placement=device,
    )
yaml = YAML(typ="safe")
with open("modelnemo/jasper10x5dr.yaml") as f:
    jasper_params = yaml.load(f)
vocab = jasper_params['labels']
sample_rate = jasper_params['sample_rate']

data_preprocessor = nemo_asr.AudioToMelSpectrogramPreprocessor(
    sample_rate=sample_rate, **jasper_params["AudioToMelSpectrogramPreprocessor"],
)
jasper_encoder = nemo_asr.JasperEncoder(
    feat_in=jasper_params["AudioToMelSpectrogramPreprocessor"]["features"], **jasper_params["JasperEncoder"],
)
jasper_decoder = nemo_asr.JasperDecoderForCTC(
    feat_in=jasper_params["JasperEncoder"]["jasper"][-1]["filters"], num_classes=len(vocab),
)
greedy_decoder = nemo_asr.GreedyCTCDecoder()


jasper_encoder.restore_from(CHECKPOINT_ENCODER)
jasper_decoder.restore_from(CHECKPOINT_DECODER)

# Instantiate necessary neural modules
data_layer = AudioDataLayer()

# Define inference DAG
audio_signal, audio_signal_len = data_layer()
processed_signal, processed_signal_len = data_preprocessor(
    input_signal=audio_signal,
    length=audio_signal_len)
encoded, encoded_len = jasper_encoder(audio_signal=processed_signal,
                                      length=processed_signal_len)
log_probs = jasper_decoder(encoder_output=encoded)
predictions = greedy_decoder(log_probs=log_probs)


def parse(AUDIO_FILE):
    _, signal = wave.read(AUDIO_FILE)
    data_layer.set_signal(signal)
    tensors = neural_factory.infer([predictions], verbose=False)
    preds = tensors[0][0]
    transcript = post_process_predictions([preds], vocab)[0]
    return transcript

# print(parse('3.wav'))
# processes = []
# num_processes = 2
# for rank in range(num_processes):
#     p = mp.Process(target=parse, args=(da,))
#     p.start()
#     processes.append(p)
# for p in processes:

# pool = Pool(processes=1)
# pool.map(parse, da)
# print('start recogn')
# for i in da:
# 	transcript = parse(i)

# print(time.time() - t)
# f = open('pidor2.txt', 'w')
# f.write(transcript)
# f.close

# print(time.time() - t)
# # print('Transcript: "{}"'.format(transcript))


