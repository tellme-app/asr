from Levenshtein import distance
from diff import diff_match_patch
import re


di = diff_match_patch()

text2 = 'how are you doing'
text1 = "how rae y o u dreo ing"

from g2p_en import G2p

g2p = G2p()

ati = {' ': ' ', 'AO': 'ɔ', 'AO0': 'ɔ', 'AO1': 'ɔ', 'AO2': 'ɔ',
 'AA': 'ɑ', 'AA0': 'ɑ', 'AA1': 'ɑ', 'AA2': 'ɑ', 'IY': 'i', 'IY0': 'i',
  'IY1': 'i', 'IY2': 'i', 'UW': 'u', 'UW0': 'u', 'UW1': 'u',
   'UW2': 'u', 'EH': 'e',  'EH1': 'e',  'EH2': 'e',  'IH': 'ɪ',
    'IH0': 'ɪ', 'IH1': 'ɪ', 'IH2': 'ɪ', 'UH': 'ʊ',  'UH0': 'ʊ',
     'UH1': 'ʊ', 'UH2': 'ʊ', 'AH': 'ʌ', 'AH0': 'ə', 'AH1': 'ʌ',
      'AH2': 'ʌ', 'AE': 'æ', 'AE0': 'æ', 'AE1': 'æ', 'AE2': 'æ',
       'AX': 'ə', 'AX0': 'ə', 'AX1': 'ə', 'AX2': 'ə', 'EY': 'eɪ',
        'EY0': 'eɪ', 'EY1': 'eɪ', 'EY2': 'eɪ', 'AY': 'aɪ', 'AY0': 'aɪ',
         'AY1': 'aɪ', 'AY2': 'aɪ', 'OW': 'oʊ', 'OW0': 'oʊ',
          'OW1': 'oʊ', 'OW2': 'oʊ', 'AW': 'aʊ', 'AW0': 'aʊ',
           'AW1': 'aʊ', 'AW2': 'aʊ', 'OY': 'ɔɪ', 'OY0': 'ɔɪ',
            'OY1': 'ɔɪ', 'OY2': 'ɔɪ', 'P': 'p', 'B': 'b', 'T': 't',
             'D': 'd', 'K': 'k', 'G': 'g', 'CH': 'tʃ', 'JH': 'dʒ',
              'F': 'f', 'V': 'v', 'TH': 'θ', 'DH': 'ð', 'S': 's',
               'Z': 'z', 'SH': 'ʃ', 'ZH': 'ʒ', 'HH': 'h', 'M': 'm',
                'N': 'n', 'NG': 'ŋ', 'L': 'l', 'R': 'r', 'ER': 'ɜr',
                 'ER0': 'ɜr', 'ER1': 'ɜr', 'ER2': 'ɜr', 'AXR': 'ər',
 'AXR0': 'ər', 'AXR1': 'ər', 'AXR2': 'ər', 'W': 'w', 'Y': 'j' }


def transcript(text):
    ph=''
    for i in g2p(text):
#         if i == ' '
        if i not in ati:
            ph+=i
        else:
            ph+=ati[i]
    return ph
# text1 = text2

# text2 = 'run'
# text1 = 'opin'

# text2 = 'hello how are you'
# text1 = 'hallhaw ar e y ou'
def check_text(text1,text2):
    tmp = ''
    tmp1 = ' '
    text1 = text1.lower()
    text2 = text2.lower()
    text1 = re.sub('[!@#$,.?\"%^&*-+]', '', text1)
    text2 = re.sub('[!@#$,.?\"%^&*-+]', '', text2)
    test = di.diff_main(text1,text2)
    print(test)
    last = False
    pre = 0
    be = 1
    lel = len(test)-1
    # test1 = []
    # for i in test:
    #     if " " in i[1] and i[0] != 0:
    #         tt = i[1].split()
    #         for j in tt:
    #             test1.append((j, i[0]))
    #             test1.append((" ", 0))
    #         test1.pop()
    #     else:
    #         test1.append(i)
    # test = test1
    # print(test)
    for i in test:


        if i[0] == 0 or (i[0] ==1 and i[1] == ' '):
            tmp += i[1]
            tmp1 += i[1]
            last = False
            pre = i[0]
        elif i[0] == -1:
            # print(tmp1)
            # print(i[1] + '///' + tmp1[-1])
            # len 

            if be <= lel and be-2 >= 0:
                if test[be][0] == 1 or test[be-2][0] == 1:
                    # print(i)
                    last = True
                    tmp+= '_'+i[1]+'_'
                else:
                    # print(tmp1[-1])
                    # print(i[1])
                    for j in i[1]:
                        tmp1+='-'
            else:
                if tmp1[-1] != ' ' and i[1][0] !=  ' ' and ' ' in i[1]:
                    # print(i[1])
                    last = True
                    tmp+= '_'+i[1]+'_'
                else:
                    # print(tmp1[-1])
                    # print(i[1])
                    for j in i[1]:
                        tmp1+='-'
            pre = i[0]
            
        elif i[0] == 1:
            
            if ' ' in i[1]:
                # print(i)
                for k in i[1].split():
                    # print(k)
                    # print(tmp1[-1])
                    if tmp1[-1] != '-' or tmp1[-1] == ' ':
                        for j in k:
                            print(j)
                            if (j == ' ' and tmp1[-1] != '-') or (j == ' ' and pre != 0):
                                tmp1+= '- '
                            else:
                                tmp1+= '-' #'('+i[1]+')'
                            pre = i[0]
                        tmp1 += ' '
            else:
                if tmp1[-1] != '-' or i[1] == ' ' or tmp1[-1] == ' ':
                    for j in i[1]:
                        # print(j)
                        if (j == ' ' and tmp1[-1] != '-') or (j == ' ' and pre != 0):
                            tmp1+= '- '
                        else:
                            tmp1+= '-' #'('+i[1]+')'
                        pre = i[0]

            last = False

        pre = i[0]
        be +=1

    if last:
        # print('last')
        tmp1 += '-'

    txt = ""
    for i in test:
        t = ""
        if i[0] == 1:
            for j in i[1]:
                if j != " ":
                    t += "-"
                else:
                    t += j
                    
        elif i[0] == -1:
            for j in i[1]:
                if j != " ":
                    t += "-"
        else:
            t += i[1]
            
        txt += t

    tempor = txt.split()
    temp = []

    tmp1 = tmp1[1:]
    for i in tmp1.split():
        if i[0] == ')':
            temp.append(i[1:])
        elif i[-1] == '(':
            temp.append(i[:-1])
        else:
            temp.append(i)

    # print(temp)
    txt = re.sub('[!@#$,.?\"%^&*-+]', '', text2).split()
    # print(txt)
    # print(temp)
    # print(tmp1.split())
    temp = tempor
    le = len(temp)

    aqq = []
    clean = 1
    print(temp)
    dem1 = []
    for i in range(le):
        dis = distance(txt[i], temp[i])
        if dis > len(txt[i]):
            aqq.append( (txt[i], 1))
            dem1.append({'text':txt[i]+" ", 'style':"accidentError"})
        elif dis == 0:
            aqq.append((txt[i], 0))
            dem1.append({'text':txt[i]+" ", 'style':"regular"})
        else:
            koef = dis/len(txt[i])
            if koef < .25:
                dem1.append({'text':txt[i]+" ", 'style':"regular"})
            elif koef >= .75:
                dem1.append({'text':txt[i]+" ", 'style':"accidentError"})
            else:
                dem1.append({'text':txt[i]+" ", 'style':"accident"})
            aqq.append((txt[i], dis/len(txt[i])))
    # print(distance(" ".join(txt), text1))
    print(dem1)
    dem = []
    for i in test:
        if i[0] == 0:
            dem.append({'text':i[1], 'style':"regular"})
        elif i[0] == 1:
            dem.append({'text':i[1], 'style':'accident'})
    # print('clean - ' + str(distance(" ".join(txt), text1) / len(text2)))
    # print(aqq)
    precisely = 1
    len_aqq = len(aqq)
    er = 0
    for i in aqq:
        er += i[1]
    precisely -= er / len_aqq
    clear = int((1 - (distance(" ".join(txt), text1) / len(text2)))*100)
    if clear < 0:
        clear = 0
    return {"clear": clear, 'precisely':int(precisely*100),"textResponse": dem1}

check_text(text1, text2)

def check_transcrip(transcript1, transcript2):
    


    test = di.diff_main(transcript1,transcript2)
    dem = []
    # dem.append({'text': "[", 'style':"regular"})

    for i in test:
        if i[0] == 0:
            dem.append({'text':i[1], 'style':"regular"})
        elif i[0] == 1:
            dem.append({'text':i[1], 'style':'accidentError'})

    # dem.append({'text': "]", 'style':"regular"})
    print(dem)
    return {"transcriptResponse": dem}

check_transcrip(transcript(text1), transcript(text2))