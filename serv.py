import os
# from nemotest import parse
from multidict import MultiDict
from pathlib import Path
from time import time, gmtime, strftime
import shutil
#!/usr/bin/python
import os
import argparse
import subprocess
import base64
import sys
from os import walk
from os import path
from multidict import MultiDict
from pathlib import Path
from time import time, gmtime, strftime
import shutil
import urllib
import subprocess
import json
import requests
import subprocess
import asyncio
import re

from IPython.display import Audio
cnt = 0

#gcp
import io
import os
import wave
import sys

sys.path.append('/home/ubuntu/asr/denoiser')

#denoiser
from denoiser.demucs import Demucs

import torch
import torchaudio
from transformers import Wav2Vec2Processor, Wav2Vec2ForCTC
from datasets import load_dataset
import soundfile as sf

processor = Wav2Vec2Processor.from_pretrained("facebook/wav2vec2-large-960h-lv60")
modelstt = Wav2Vec2ForCTC.from_pretrained("facebook/wav2vec2-large-960h-lv60").to("cuda")

from denoiser.audio import Audioset
from denoiser import distrib, pretrained
from denoiser.demucs import DemucsStreamer
import logging
from denoiser.utils import LogProgress
logger = logging.getLogger(__name__)

model = Demucs(hidden=64)

pkg = torch.load('denoiser/model/master64-8a5dfb4bb92753dd.th')
model.load_state_dict(pkg)
model.to('cuda')
model.eval()

def find_audio_files(audio_file, progress=True):
#     audio_files = []
#     for root, folders, files in os.walk(path, followlinks=True):
#         for file in files:
#             file = Path(root) / file
#             if file.suffix.lower() in exts:
#                 audio_files.append(str(file.resolve()))
    meta = []
    audio_files = [str(Path(audio_file).resolve())]
    for idx, file in enumerate(audio_files):
        siginfo = torchaudio.info(file)
        length = siginfo.num_frames // siginfo.num_channels
        meta.append((file, length))
        if progress:
            print(format((1 + idx) / len(audio_files), " 3.1%"), end='\r', file=sys.stderr)
    meta.sort()
    return meta


def get_estimate(model, noisy):
    torch.set_num_threads(1)
    if False:
        pass
#         streamer = DemucsStreamer(model, dry=args.dry)
#         with torch.no_grad():
#             estimate = torch.cat([
#                 streamer.feed(noisy[0]),
#                 streamer.flush()], dim=1)[None]
    else:
        with torch.no_grad():
            estimate = model(noisy)
            estimate = (1 - 0) * estimate + 0 * noisy
    return estimate


def save_wavs(estimates, noisy_sigs, filenames, out_dir, sr=16_000):
    # Write result
    for estimate, noisy, filename in zip(estimates, noisy_sigs, filenames):
        filename = os.path.join(out_dir, os.path.basename(filename).rsplit(".", 1)[0])
        write(noisy, filename + "_noisy.wav", sr=sr)
        write(estimate, filename + "_enhanced.wav", sr=sr)


def write(wav, filename, sr=16_000):
    # Normalize audio if it prevents clipping
    wav = wav / max(wav.abs().max().item(), 1)
    torchaudio.save(filename, wav.cpu(), sr)


def enhance(file, model=None, local_out_dir=None):
    # Load model

    if local_out_dir:
        out_dir = local_out_dir
        
    files = find_audio_files(file)
    print(files)

    dset = Audioset(files, with_path=True, sample_rate=16_000)
    print(dset)
    if dset is None:
        return
    loader = distrib.loader(dset, batch_size=1)
    
    if distrib.rank == 0:
        os.makedirs(out_dir, exist_ok=True)
    distrib.barrier()

    with torch.no_grad():
        iterator = LogProgress(logger, loader, name="Generate enhanced files")
        for data in iterator:
            # Get batch data
            noisy_signals, filenames = data
            noisy_signals = noisy_signals.to('cuda')
            # Forward
            estimate = get_estimate(model, noisy_signals)
            save_wavs(estimate, noisy_signals, filenames, out_dir, sr=16_000)

def map_to_array(file):
    speech, _ = sf.read(file)
    # batch["speech"] = speech
    return speech

def new_stt(file):
    map_to_array(file)
    input_values = processor(speech, return_tensors="pt").input_values.to("cuda")  # Batch size 1
    logits = modelstt(input_values).logits
    predicted_ids = torch.argmax(logits, dim=-1)

    transcription = processor.decode(predicted_ids[0])

# web
from aiohttp import web
import aiohttp_cors


from dit import check_text, check_transcrip, transcript

here = Path(__file__).resolve().parent

routes = web.RouteTableDef()



@routes.post('/transcript')
async def store_wav(request):
    data = await request.json()

    text = data['text']

    ph = transcript(text)

    return web.json_response({"transcription": ph})


# @routes.post('/get_enhanced')
# async def store_wav(request):
#     redis_get_url = 'http://40.114.54.96:8003/get_audio'
#     data = await request.json()

#     idenh = data['id']

#     ph = transcript(text)

#     return web.json_response({"transcription": ph})



@routes.post('/stt')
async def store_wav(request):
    global cnt
    # redis_set_url = 'http://40.114.54.96:8003/set_audio'

    t2 = time()
    print(requests)

    reader = await request.multipart()
    print(reader  )

    field = await reader.next()
    #assert field.name == 'name'
    print(field)
    text = None
    nex = True
    try:
        data = await field.json() 
    except:
        nex = False

    if nex:
        print(data)
        
        if 'text' in data:
            text = data['text']
        
        field = await reader.next()


        if text is not None:
            filename = field.filename
            filename = text+"_"+filename
        else:
            filename = field.filename

        print(filename)
        data_f = await field.read(decode=True)
    else:
        # field = await reader.next()
        filename = "huhi"
        data_f = await field.read(decode=True)



    filename = 'audio/tmpfile_'+ filename.replace(' ', '_')
    filename = re.sub('[!@#$,?\'\"%^&*-+]', '', filename.lower())
    file1 = open(filename,'wb')
    file1.write(data_f)
    file1.close()
    print(filename)


    # print(time() - t2)
    # print("---------------------")
    cmd = " ".join(['ffmpeg','-y', '-i', filename, '-acodec', 'pcm_s16le', '-ac', '1',
                         '-ar', '16000', filename.split(".")[0]+".wav"])
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    enhance(filename.split(".")[0]+".wav",model,'out_noise/' )
    filename = filename.split(".")[0]+"_enhanced.wav"
    filename = "out_noise/"+filename.split('/')[1]
    audio = Audio(filename)
    audio = audio.data
    cnt+=1
    # requests.post(redis_set_url, files={"file":audio}, data={"data":json.dumps({'text':str(cnt)})})

    print(time() - t2)
    print("---------------------")
    # f = wave.open(filename, 'rb')
    t = time()
    # recognized = parse(filename)
    recognized = new_stt(filename)

    if text is not None:
        text = text.lower()
        text = re.sub('[!@#$,.?\"%^&*-+]', '', text)
        trans = transcript(text)
        print(trans)
        rec_trans = transcript(recognized)
        print(rec_trans)
        ret = check_text(recognized,text)
        ret['transcriptResponse'] = check_transcrip(rec_trans,trans)['transcriptResponse']
    else:
        ret = {"data": recognized}
    ret['id_enhanced'] = str(cnt)
    print(time()-t)
    print(ret)
    
    return web.json_response(ret)


@routes.post('/stturl')
async def store_wav(request):
    global cnt
    data = await request.json()
    # redis_set_url = 'http://40.114.54.96:8003/set_audio'

    t2 = time()
    r = requests.get(data['url'])
    text = None
    
    data_f = r.content

    filename = 'audio/tmpfile_'+ str(cnt)
    filename = re.sub('[!@#$,?\'\"%^&*-+]', '', filename.lower())
    file1 = open(filename,'wb')
    file1.write(data_f)
    file1.close()
    print(filename)


    # print(time() - t2)
    # print("---------------------")
    cmd = " ".join(['ffmpeg','-y', '-i', filename, '-acodec', 'pcm_s16le', '-ac', '1',
                         '-ar', '16000', filename.split(".")[0]+".wav"])
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)

    stdout, stderr = await proc.communicate()

    enhance(filename.split(".")[0]+".wav",model,'out_noise/' )
    filename = filename.split(".")[0]+"_enhanced.wav"
    filename = "out_noise/"+filename.split('/')[1]
    audio = Audio(filename)
    audio = audio.data
    cnt+=1
    # requests.post(redis_set_url, files={"file":audio}, data={"data":json.dumps({'text':str(cnt)})})
    
    print(time() - t2)
    print("---------------------")
    # f = wave.open(filename, 'rb')
    t = time()
    recognized = parse(filename)

    if text is not None:
        text = text.lower()
        text = re.sub('[!@#$,.?\"%^&*-+]', '', text)
        trans = transcript(text)
        print(trans)
        rec_trans = transcript(recognized)
        print(rec_trans)
        ret = check_text(recognized,text)
        ret['transcriptResponse'] = check_transcrip(rec_trans,trans)['transcriptResponse']
    else:
        ret = {"data": recognized}
    ret['id_enhanced'] = str(cnt)
    print(time()-t)
    print(ret)
    
    return web.json_response(ret)

app = web.Application()
# aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(str(here)))
app.add_routes(routes)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
})
# app.router.add_post('/', store_mp3_handler)
for route in list(app.router.routes()):
    cors.add(route)
#web.run_app(app)
web.run_app(app,host='0.0.0.0', port = '8002')

    